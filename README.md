### Connect Angular to a Node Server Hello World

#Requirements
Node 6+

NPM 4+
#

##Angular
Move in the Angular folder: `cd Angular`

Install all the nodes modules: `npm install`

Run the angular module: `npm start`

##Node
Move in the Node folder: `cd Node`

To run the node module: `node bin/www` (or windows `node bin\www`)

##Run app
Go to this url: http://localhost:4200
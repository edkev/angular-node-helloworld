import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";

@Injectable()
export class AppService {

  constructor(public http: Http) { }

  connectNode() {
    return this.http.get('http://localhost:3000/users/test')
      .map(response => response)
  }
}
